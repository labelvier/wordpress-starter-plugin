document.addEventListener("DOMContentLoaded", initLazyListener, { once: true });

window.onload = function() {
    if (window.jQuery) {
        // jQuery is loaded
        jQuery(document).on('facetwp-loaded', initLazyListener);
        jQuery( document ).ajaxComplete(function( event, xhr, settings ) {
            initLazyListener();
        });
    }
    document.addEventListener('facetwp-loaded', function() {
        initLazyListener();
    });
};
export function initLazyListener() {
    const supports = checkSupport();
    var lazyImages = [].slice.call(document.querySelectorAll(".js-lazy"));
    if ("IntersectionObserver" in window) {

        const observerOptions = {
            root: null, // viewport
            rootMargin: "100px", // pixeloffset outside frame
            // threshold: 0.1 // when 50% of element visible
        };

        if(labelvier.lazyImageObserver) {
            labelvier.lazyImageObserver.disconnect();
            labelvier.lazyImageObserver = false;
        }

        labelvier.lazyImageObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (entry) {

                if (entry.isIntersecting) {

                    let lazyImageWrap = entry.target;
                    let lazyImage = entry.target.classList.contains('js-lazy') && !entry.target.classList.contains('loading') && !entry.target.classList.contains('loaded') ? entry.target : false;

                    if (lazyImage) {

                        // remove the attribute lazy, if it is set
                        if (lazyImage.hasAttribute('lazy')) {
                            lazyImage.removeAttribute('lazy');
                        }

                        if (supports.srcset) {
                            lazyImage.srcset = lazyImage.dataset.labelvier_srcset;
                            if (lazyImage.srcset.length === 0) {
                                //fall back to source with no srcset
                                lazyImage.src = lazyImage.dataset.labelvier_src;
                            }
                        } else {
                            lazyImage.src = lazyImage.dataset.labelvier_src;
                        }
                        lazyImageWrap.classList.remove("lazy");
                        lazyImageWrap.classList.add("animate");
                        lazyImageWrap.classList.add("loading");
                        labelvier.lazyImageObserver.unobserve(lazyImageWrap);

                        lazyImage.onload = function () {
                            setTimeout(function () {
                                lazyImageWrap.classList.remove("loading");
                                lazyImageWrap.classList.add('loaded');
                            }, 60);
                        }
                    }
                    else {
                        labelvier.lazyImageObserver.unobserve(lazyImageWrap);
                    }
                }
            });
        }, observerOptions);
        lazyImages.forEach(function (lazyImage) {
            labelvier.lazyImageObserver.observe(lazyImage);
        });
    } // Possibly fall back to a more compatible method here. But we are loading a Polyfill - so we do nothing
}

function checkSupport() {
    const supports = {
        srcset: false,
        currentSrc: false,
        sizes: false,
        picture: false
    };

    let img = new Image();

    if ("srcset" in img) {
        supports.srcset = true;
    }

    if ("currentSrc" in img) {
        supports.currentSrc = true;
    }

    if ("sizes" in img) {
        supports.sizes = true;
    }

    if ("HTMLPictureElement" in window) {
        supports.picture = true;
    }

    // console.log('Picture support: ' + supports.picture);
    // console.log('sizes support: ' + supports.sizes);
    // console.log('srcset support: ' + supports.srcset);
    // console.log('currentSrc support: ' + supports.currentSrc);

    return supports;
}
