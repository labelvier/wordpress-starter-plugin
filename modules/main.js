/**
 * Import the module scripts
 */
window.labelvier = window.labelvier || {};
import './polyfills/foreach';
import './polyfills/intersection-observer';
import {initLazyListener} from "./lazy-loading/lazy-loading";
import './spam-filter/spam-filter';

window.labelvier.initLazyListener = initLazyListener;
