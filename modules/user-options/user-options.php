<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 26-07-2022
 * Time: 09:13
 */

if(!wp_is_file_mod_allowed('allow_administrator_file_mod')) {
	/**
	 * Allow file modification if the user has the correct meta key.
	 */
	add_action( 'init', function () {
		// check if ACF is enabled
		if ( ! function_exists( 'get_field' ) ) {
			return;
		}
		// check if there is a user logged in, and if so get the meta value file_mod_allowed for the current user
		if ( is_user_logged_in() ) {
			$user             = wp_get_current_user();
			$file_mod_allowed = get_user_meta( $user->ID, 'file_mod_allowed', true );
			if ( $file_mod_allowed === '1' ) {
				add_filter( 'file_mod_allowed', '__return_true' );
			}
		}
	} );

	/**
	 * Add a capability group to the user options page
	 */
	if ( function_exists( 'acf_add_local_field_group' ) ):

		acf_add_local_field_group( array(
			'key'                   => 'group_62df93f316f61',
			'title'                 => 'Capabilities',
			'fields'                => array(
				array(
					'key'               => 'field_62df942f8fb5e',
					'label'             => 'File mod allowed',
					'name'              => 'file_mod_allowed',
					'type'              => 'true_false',
					'instructions'      => 'This allows this user to edit and install plugin / themes / etc. Handle with care',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'message'           => 'Allow this user to update / install files on WordPress',
					'default_value'     => 0,
					'ui'                => 0,
					'ui_on_text'        => '',
					'ui_off_text'       => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'current_user_role',
						'operator' => '==',
						'value'    => 'administrator',
					),
					array(
						'param' => 'user_form',
						'operator' => '==',
						'value' => 'all',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'field',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
			'show_in_rest'          => 0,
		) );

	endif;
}
