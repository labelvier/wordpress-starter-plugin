<?php
/**
 * Remove the default welcome dashboard message and replaces it with a custom labelvier version
 * @package LabelVier
 * @author Nathanael
 */

/**
 * Remove the default welcome panel
 */
remove_action( 'welcome_panel', 'wp_welcome_panel' );


/**
 * Disable Default Dashboard Widgets
 */
if(!function_exists('disable_default_dashboard_widgets')) {
    function disable_default_dashboard_widgets()
    {
        global $wp_meta_boxes;

        // WordPress defaults
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
        unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
        unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);

        // Yoast
        // unset( $wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget'] );

        // Gravity Forms
        // unset( $wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard'] );
    }
}
add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets', 999 );


/**
 * Custom dashboard blocks
 */
function labelvier_dashboard_widgets() {
    add_meta_box( 'labelvier_dashboard_control', 'Direct aan de slag', 'labelvier_dashboard_control', 'dashboard', 'normal', 'high' );
    add_meta_box( 'labelvier_dashboard_help', 'label<strong>vier</strong> support', 'labelvier_dashboard_help', 'dashboard', 'normal', 'high' );
    add_meta_box( 'labelvier_blogroll', 'label<strong>vier</strong> blog', 'labelvier_blogroll', 'dashboard', 'side', 'high' );
}
add_action( 'wp_dashboard_setup', 'labelvier_dashboard_widgets' );

function labelvier_dashboard_help() {
    if ($basecampLink = get_field('basecamp_project_link', 'options')) {
        echo '<p>Heb je hulp nodig? Neem dan contact met ons op! Je kunt ons bereiken in onze projectmanagement software <a href="'; echo $basecampLink; echo '" target="_blank">Basecamp</a>. Heb je geen toegang tot Basecamp? Stuur ons dan <a href="mailto:hallo@labelvier.nl" target="_blank" rel="nofollow">een mailtje</a>.</p>';
        echo '<p>Uitleg over WordPress nodig? Je vindt hier de uitgebreide <a href="https://www.wpbeginner.com" target="_blank">online handleiding</a> voor WordPress.</p>';
    } else {
        echo '<p>Heb je hulp nodig? Neem dan <a href="mailto:hallo@labelvier.nl" target="_blank" rel="nofollow">contact</a> met ons op!  Uitleg over WordPress nodig? Je vindt hier de uitgebreide <a href="https://wordpress.org/support/" target="_blank" rel="nofollow">online handleiding</a> voor WordPress.';
    }
}

function labelvier_dashboard_control() { ?>
    <ul class="quick-links-panel">
        <li><?php printf( '<a href="%s" class="icon welcome-add-page">' . __( 'Maak nieuwe pagina' ) . '</a>', admin_url( 'post-new.php?post_type=page' ) ); ?></li>
        <li><?php printf( '<a href="%s" class="icon welcome-add-post">' . __( 'Maak nieuw bericht' ) . '</a>', admin_url( 'post-new.php?post_type=post' ) ); ?></li>
        <li><?php printf( '<a href="%s" class="icon welcome-change-menu">' . __( 'Menu wijzigen' ) . '</a>', admin_url( 'nav-menus.php' ) ); ?></li>
        <li><?php printf( '<a href="%s" class="icon welcome-theme-settings">' . __( 'Thema instellingen' ) . '</a>', admin_url( 'themes.php?page=theme-settings' ) ); ?></li>
        <li><?php printf( '<a href="%s" class="icon welcome-view-site" target="_blank">' . __( 'Bekijk de site' ) . '</a>', home_url( '/' ) ); ?></li>
    </ul>
    <style>
        .quick-links-panel {
            margin-bottom: -10px;
        }
        .quick-links-panel li {
            line-height: 1.14285714;
            list-style-type: none;
            padding: 0 0 8px;
        }
        .quick-links-panel .icon:before {
            color: #646970;
            font: normal 20px/1 dashicons;
            speak: never;
            display: inline-block;
            padding: 0 10px 0 0;
            position: relative;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            text-decoration: none!important;
            vertical-align: top;
        }
        .quick-links-panel .welcome-add-page:before {
            content: "\f133";
            top: -3px;
        }
        .quick-links-panel .welcome-add-post:before {
            content: "\f109";
            top: -3px;
        }
        .quick-links-panel .welcome-view-site:before {
            content: "\f115";
            top: -2px;
        }
        .quick-links-panel .welcome-change-menu:before {
            content: "\f333";
            top: -2px;
        }
        .quick-links-panel .welcome-theme-settings:before {
            content: "\f111";
            top: -3px;
        }
    </style>
    <?php
}

function labelvier_blogroll() { ?>
    <div id="labelvierblog"></div>
    <style>
        #welcome-panel,
        #dashboard-widgets-wrap .postbox,
        #labelvier_blogroll,
        #labelvier_blogroll .postbox-header {
            border: none;
        }
        #dashboard-widgets-wrap .postbox .postbox-header {
            background: #fafafa;
            border: none;
        }
        #labelvier_dashboard_help .postbox-header,
        #labelvier_blogroll .postbox-header {
            background: #e06561 !important;
        }
        #labelvier_dashboard_help .postbox-header h2,
        #labelvier_blogroll .postbox-header h2 {
            justify-content: unset;
            font-weight: normal;
        }
        #labelvier_dashboard_help .postbox-header h2 strong,
        #labelvier_blogroll .postbox-header h2 strong {
            font-weight: bold;
            margin-right: 6px;
        }
        #labelvier_dashboard_help .postbox-header *,
        #labelvier_blogroll .postbox-header * {
            color: #fff;
        }
    </style>
    <script>
        jQuery(document).ready(function () {
            jQuery.ajax({
                type: "POST",
                url: "/wp-admin/admin-ajax.php",
                data: {
                    action: "labelvier"
                }
            }).done(function (data) {
                jQuery("#labelvierblog").html(data);
            });
        })
    </script> <?php
}


function show_rss_forum_feed() { ?>
    <p class="l4-feed__header">Onze laatste artikelen voor jou:</p>
    <ul class="l4-feed">
        <?php include_once(ABSPATH . WPINC . '/feed.php');
        $feed = fetch_feed('https://labelvier.nl/feed/');
        $limit = 5;
        $items = $feed->get_items(0, $limit);
        if ($limit == 0) echo '<li>Geen berichten.</li>';
        else foreach ($items as $item) : ?>
            <li class="l4-feed__item">
                <a href="<?= $item->get_permalink() ?>" target="_blank" rel="nofollow" class="l4-feed__link">
                    <p class="l4-feed__date hidden"><?= $item->get_date('j F Y @ g:i a') ?></p>
                    <p class="l4-feed__title"><?= $item->get_title() ?></p>
                    <p class="l4-feed__text hidden"><?= strip_tags(substr($item->get_description(), 0, 100)) ?>
                        <span>[...]</span>
                    </p>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <p><a href="https://labelvier.nl/blog/" target="_blank" rel="nofollow">Laat me meer zien</a></p>
    <style>
        .l4-feed__item {
            margin-left: 17px;
            list-style: disc;
        }
    </style>
    <?php
    wp_die();
}
add_action('wp_ajax_labelvier', 'show_rss_forum_feed'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_fietsforum', 'show_rss_forum_feed'); // Fires non-authenticated Ajax actions for logged-out users.



/**
 * Dashboard options
 */

add_filter( 'labelvier_theme_settings_fields', function ( $fields ) {
    $dashboard_fields = [
        array(
            'key'               => 'field_5e32a9aead72e',
            'label'             => 'Welcome Dashboard',
            'name'              => '',
            'type'              => 'tab',
            'instructions'      => '',
            'required'          => 0,
            'conditional_logic' => 0,
            'wrapper'           => array(
                'width' => '',
                'class' => '',
                'id'    => '',
            ),
            'placement'         => 'top',
            'endpoint'          => 0,
        ),
        array(
            'key'               => 'field_61b8b60365222',
            'label'             => 'Basecamp project link',
            'name'              => 'basecamp_project_link',
            'type'              => 'text',
            'instructions'      => '',
            'required'          => 0,
            'conditional_logic' => 0,
            'wrapper'           => array(
                'width' => '',
                'class' => '',
                'id'    => '',
            ),
            'default_value'     => '',
            'placeholder'       => '',
            'prepend'           => '',
            'append'            => '',
            'maxlength'         => '',
        ),
    ];
    $fields           = array_merge( $fields, $dashboard_fields );

    return $fields;
}, 30, 1 );
