<?php
// WooPayments Removal from the main menu //
function remove_wc_payments_ad_tab() {
    remove_menu_page('admin.php?page=wc-admin&task=payments');
    remove_menu_page('admin.php?page=wc-admin&task=woocommerce-payments');
    remove_menu_page('admin.php?page=wc-settings&tab=checkout');
}
add_action('admin_menu', 'remove_wc_payments_ad_tab', 999);
