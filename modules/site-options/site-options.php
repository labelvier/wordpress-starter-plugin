<?php
/**
 * Option pages.
 *
 * @package labelvier
 */

/**
 * Init the acf settings page when there is not one yet...
 */
function labelvier_init_theme_settings_pages_from_plugin() {
	if ( function_exists( 'acf_add_options_sub_page' ) ) {
		$option_page = acf_add_options_sub_page(
			array(
				'page_title'  => 'Label Vier Extra\'s',
				'menu_title'  => 'Label Vier Extra\'s',
				'menu_slug'   => 'labelvier-extras',
				'parent_slug' => 'options-general.php',
				'capability'  => 'edit_posts',
				'redirect'    => false,
				'position'    => 9
			)
		);
	}

	/**
	 * Add the settings fields with ACF PHP logic. You can add your extra optional fields via the filter (or change them here).
	 */
	if ( function_exists( 'acf_add_local_field_group' ) ):
		$fields = apply_filters( 'labelvier_theme_settings_fields', []);

		acf_add_local_field_group( array(
			'key'                   => 'group_5daef4421c8c7-php',
			'title'                 => 'Label Vier Extra\'s',
			'fields'                => $fields,
			'location'              => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'labelvier-extras',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'seamless',
			'label_placement'       => 'left',
			'instruction_placement' => 'field',
			'hide_on_screen'        => '',
			'active'                => true,
			'description'           => '',
		) );

	endif;
}

add_action('acf/init', 'labelvier_init_theme_settings_pages_from_plugin', 11);
