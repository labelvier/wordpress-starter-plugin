<?php

add_filter('trp_settings_tabs', function ( $tabs ) {
	// check if we have a deepl api key
	$machine_translation_settings = get_option( 'trp_machine_translation_settings', '' );
	$deepl_api_key                = $machine_translation_settings['deepl-api-key'] ?? '';
	if(!$deepl_api_key) {
		return $tabs;
	}
	$tabs[] = [
		'name' => 'Glossary',
		'page' => 'deepl_glossary',
		'url' => admin_url('admin.php?page=deepl_glossary')
	];
	return $tabs;
}, 10, 1);

// register the new page
add_action('admin_menu', function () {
	add_submenu_page(
		'translatepress-multilingual',
		'DeepL Glossary',
		'DeepL Glossary',
		'manage_options',
		'deepl_glossary',
		static function () {
            require_once __DIR__ . '/template-parts/deepl-glossary.php';
		}
	);
});

function deepl_create_glossary($from, $to, $entries) {
	// trp_machine_translation_settings[deepl-api-key]
	$machine_translation_settings = get_option( 'trp_machine_translation_settings', '' );
	$deepl_api_key                = $machine_translation_settings['deepl-api-key'] ?? '';

	// id consists of url, site title and 8 random characters
	$deepl_glossary_name = get_site_url() . ' - ' . get_bloginfo( 'name' ) . ' - ' . $from . '-' . $to . ' - ' . wp_generate_password( 8, false );
	// do a wp_remote_post to create a glossary
	$args = [
		'headers' => [
			'Authorization' => 'DeepL-Auth-Key ' . $deepl_api_key
		],
		'body'    => [
			'name'           => $deepl_glossary_name,
			'source_lang'    => $from,
			'target_lang'    => $to,
			'entries'        => implode("\n", $entries),
			'entries_format' => 'tsv'
		]
	];
	$response = wp_remote_post( 'https://api.deepl.com/v2/glossaries', $args);
	// check if the response is a success
	if ( ! is_wp_error( $response ) && in_array( $response['response']['code'], [ 200, 201 ] ) ) {
		$response_body = json_decode( $response['body'], true );
		return $response_body['glossary_id'];
	} else {
		$response_body = json_decode( $response['body'], true );
		if($response_body && !empty($response_body['message'])) {
			$message = 'Could not create or update glossary on DeepL - ' . $response_body['message'];
			if(!empty($response_body['detail'])) {
				$message .= ' (' . $response_body['detail'] . ')';
			}
			return new WP_Error('deepl_error', $message, $response);
		}
		print_pre($response);
		wp_die( 'Could not create glossary on DeepL' );
	}
}

function deepl_delete_glossary($glossary_id) {
	// trp_machine_translation_settings[deepl-api-key]
	$machine_translation_settings = get_option( 'trp_machine_translation_settings', '' );
	$deepl_api_key                = $machine_translation_settings['deepl-api-key'] ?? '';
	// delete the old glossary
	$response = wp_remote_request( 'https://api.deepl.com/v2/glossaries/' . $glossary_id, [
		'method' => 'DELETE',
		'headers' => [
			'Authorization' => 'DeepL-Auth-Key ' . $deepl_api_key
		]
	] );
	if(is_wp_error($response) || $response['response']['code'] !== 204) {
		$response_body = json_decode( $response['body'], true );
		if($response_body && !empty($response_body['message'])) {
			return new WP_Error('deepl_error', $response_body['message'], $response);
		}
		print_pre($response);
		wp_die( 'Could not delete glossary on DeepL' );
	}
	return true;
}

function deeple_glossary_get_entries($glossary_id) {
	// trp_machine_translation_settings[deepl-api-key]
	$machine_translation_settings = get_option( 'trp_machine_translation_settings', '' );
	$deepl_api_key                = $machine_translation_settings['deepl-api-key'] ?? '';

	// get the glossary
	$entries = wp_remote_get( 'https://api.deepl.com/v2/glossaries/' . $glossary_id . '/entries', [
		'headers' => [
			'Authorization' => 'DeepL-Auth-Key ' . $deepl_api_key
		]
	] );

	if ( is_wp_error( $entries ) || $entries['response']['code'] !== 200 ) {
		$response_body = json_decode( $response['body'], true );
		if($response_body && !empty($response_body['message'])) {
			return new WP_Error('deepl_error', $response_body['message'], $response);
		}
		print_pre($response);
		wp_die( 'Could not create glossary on DeepL' );
	}
	// get the body, and explode on line breaks and tabs
	return explode( "\n", $entries['body'] );
}

/**
 * Add glossary id to DeepL requests
 */
add_filter('http_request_args', function($args, $url) {
    if( str_contains( $url, 'deepl.com/v2/translate' ) ) {
        // check if we have a glossary for this language pair (source_lang to target_lang)
        if(empty($args['body'])) {
            return $args;
        }
        // body are url params
        parse_str($args['body'], $parsed_body);
        if( isset( $parsed_body['target_lang'], $parsed_body['source_lang'] ) ) {
            $glossary_option_key = 'deepl-glossary-' . $parsed_body['source_lang'] . '-' . $parsed_body['target_lang'];
            $glossary_id = get_option($glossary_option_key, false);
            if($glossary_id) {
                $args['body'] .= '&glossary_id=' . $glossary_id;
            }
        }
    }
    return $args;
}, 10, 2);
