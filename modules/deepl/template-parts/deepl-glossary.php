<div id="trp-main-settings" class="wrap">
    <form method="post" action="admin.php?page=deepl_glossary">
		<?php settings_fields( 'trp_machine_translation_settings' ); ?>
        <h1> <?php esc_html_e( 'Deepl glossary', 'translatepress-multilingual' ); ?></h1>
		<?php
		do_action( 'trp_settings_navigation_tabs' ); ?>
        <p class="trp-main-description">This page allows you to manage the glossary of your DeepL account. The glossary
            is a list of words and phrases that DeepL will always translate in a specific way. You can add or remove
            words and phrases from the glossary.</p>
		<?php
		$free_version = ! class_exists( 'TRP_Handle_Included_Addons' );
		$seo_pack_active = class_exists( 'TRP_IN_Seo_Pack' );
		$languages = trp_get_languages();
		// trp_machine_translation_settings[deepl-api-key]
		$machine_translation_settings = get_option( 'trp_machine_translation_settings', '' );
		$deepl_api_key = $machine_translation_settings['deepl-api-key'] ?? '';
		$from = array_keys( $languages )[0];
		$from_short = explode( '_', $from )[0];
		$from_label = array_shift( $languages );

		foreach ( $languages as $language_code => $language_name ) :
		$to = $language_code;
		$to_label = $language_name;
		$to_short = explode( '_', $to )[0];
		$glossary_option_key = 'deepl-glossary-' . $from_short . '-' . $to_short;

		// check if we have a glossary
		$current_glossary_id = get_option( $glossary_option_key, false );

		// if we have a post request, update the glossary
		if ( isset( $_POST[ $glossary_option_key ] ) ) {
			$entries = [];
			foreach ( $_POST[ $glossary_option_key ] as $item ) {
				if ( isset( $item['source'] ) ) {
					$source = trim( $item['source'] );
				}
				if ( isset( $item['target'] ) ) {
					// escape tabs and new lines in the source and target
					$source         = str_replace( [ "\t", "\n" ], [ '\\t', '\\n' ], $source );
					$item['target'] = str_replace( [ "\t", "\n" ], [ '\\t', '\\n' ], $item['target'] );
					$entries[]      = $source . "\t" . trim( $item['target'] );
					$source         = null;
				}
			}
			$glossary_id = deepl_create_glossary( $from_short, $to_short, $entries );
			if ( ! is_wp_error( $glossary_id ) ) {
				update_option( $glossary_option_key, $glossary_id );
				if ( $current_glossary_id ) {
					deepl_delete_glossary( $current_glossary_id );
					if ( is_wp_error( $glossary_id ) ) {
						// echo the error message
						echo '<div class="notice notice-error">
                             <p>' . $glossary_id->get_error_message() . '</p>
                             <a href="#" onclick="deeplShowErrorDetails(this);return false;">Show detail</a>
                             <div class="error-data" style="display: none;">
                                ' . print_pre( $glossary_id->get_error_data(), true ) . '
                                </div>
                            </div>';
					}
				}
				$current_glossary_id = $glossary_id;
			} else {
				// echo the error message
				echo '<div class="notice notice-error">
                             <p>' . $glossary_id->get_error_message() . '</p>
                             <a href="#" onclick="deeplShowErrorDetails(this);return false;">Show detail</a>
                             <div class="error-data" style="display: none;">
                             ' . print_pre( $glossary_id->get_error_data(), true ) . '
                             </div>
                            </div>';
			}
		}
		if($current_glossary_id) {
			$entries = deeple_glossary_get_entries( $current_glossary_id );
			if ( is_wp_error( $entries ) ) {
				// echo the error message
				echo '<div class="notice notice-error">
                             <p>' . $entries->get_error_message() . '</p>
                             <a href="#" onclick="deeplShowErrorDetails(this);return false;">Show detail</a>
                             <div class="error-data" style="display: none;">
                                ' . print_pre( $entries->get_error_data(), true ) . '
                                </div>
                    </div>';
				$entries = null;
			}
			if ( empty( $entries ) ) {
				$entries = [ " \t " ];
			}
		} else {
            $entries = [ " \t " ];
        }
		?>
        <table class="form-table trp-machine-translation-options <?= $glossary_option_key ?>">
            <tr>
                <th scope="row"><?php esc_html_e( "Glossary from $from_label to $to_label", 'translatepress-multilingual' ); ?> </th>
                <td>
                    <table>
                        <tr>
                            <th><?php esc_html_e( 'Source (' . $from_short . ')', 'translatepress-multilingual' ); ?></th>
                            <th><?php esc_html_e( 'Target (' . $to_short . ')', 'translatepress-multilingual' ); ?></th>
                            <th><?php esc_html_e( 'Action', 'translatepress-multilingual' ); ?></th>
                        </tr>
						<?php foreach ( $entries as $glossary_index => $glossary_item ) :
							$glossary_item = explode( "\t", $glossary_item );
							?>
                            <tr>
                                <td>
                                    <input required type="text"
                                           name="<?= $glossary_option_key ?>[][source]"
                                           value="<?php echo esc_attr( $glossary_item[0] ); ?>">
                                </td>
                                <td>
                                    <input required type="text"
                                           name="<?= $glossary_option_key ?>[][target]"
                                           value="<?php echo esc_attr( $glossary_item[1] ); ?>">
                                </td>
                                <td>
                                    <a href="#" class="button trp-remove-row"
                                       onclick="deeplGlossaryDeleteRow('<?= $glossary_option_key ?>', this);"><?php esc_html_e( 'Remove', 'translatepress-multilingual' ); ?></a>
                                </td>
                            </tr>
						<?php endforeach; ?>
                    </table>
                    <a href="#" class="button trp-add-row"
                       onclick="deeplGlossaryCopyRow('<?= $glossary_option_key ?>');"><?php esc_html_e( 'Add Row', 'translatepress-multilingual' ); ?></a>
                </td>
            </tr>
        </table>
        <?php endforeach; ?>
        <p class="description">
			<?php esc_html_e( 'The glossay will only be used in new translations. Existing translations will not be affected.', 'translatepress-multilingual' ); ?>
        </p>
        <p class="submit"><input type="submit" class="button-primary"
                                 value="<?php esc_attr_e( 'Save Changes', 'translatepress-multilingual' ); ?>"/></p>
    </form>
</div>

<script>
    function deeplShowErrorDetails(element) {
        const errorData = element.nextElementSibling;
        if(!errorData) {
            return;
        }
        if (errorData.style.display === 'none') {
            errorData.style.display = 'block';
        } else {
            errorData.style.display = 'none';
        }
    }

    function deeplGlossaryCopyRow(selector) {
        var table = document.querySelector('table.' + selector + ' table');
        var newRow = table.rows[1].cloneNode(true);
        var sourceInput = newRow.querySelector('input[name^="' + selector + '[][source]"]');
        var targetInput = newRow.querySelector('input[name^="' + selector + '[][target]"]');
        sourceInput.value = '';
        targetInput.value = '';
        table.appendChild(newRow);
    }

    function deeplGlossaryDeleteRow(selector, element) {
        var table = document.querySelector('table.' + selector + ' table');
        if (table.rows.length > 2) {
            // remove the row
            const row = element.closest('tr');
            row.parentNode.removeChild(row);
        }
    }
</script>