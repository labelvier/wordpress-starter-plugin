<?php
// Check if the is_develop_environment function exists, if not, define it
function l4_is_develop_environment() {
    return defined( 'WP_ENV' ) && WP_ENV === 'develop';
}
