<?php

if(isset($_GET['force_https'])) {
    add_action('init', function () {
        if(l4_is_develop_environment()) {
            wp_die('Force HTTPS not added to .htaccess because we are in a develop environment');
        }
        $success = labelvier_install_force_https();
        wp_die('Force HTTPS ' . ($success ? 'added' : 'not added') . ' to .htaccess');
    });
}

/**
 * Schedule an action with the hook 'eg_midnight_log' to run at midnight each day
 * so that our callback is run then.
 */
function wt_schedule_force_https() {
    if(!function_exists('as_has_scheduled_action')) {
        return;
    }
    if ( false === as_has_scheduled_action( 'wt_schedule_force_https' ) ) {
        as_schedule_recurring_action( strtotime( 'tomorrow' ), DAY_IN_SECONDS, 'wt_check_force_https', array(), '', true );
    }
}
add_action( 'init', 'wt_schedule_force_https' );

// run the force https check when the scheduled action is triggered
add_action('wt_check_force_https', 'labelvier_install_force_https');

// check if we have a .htaccess file in the root of the wordpress installation and if so, add the force https snippet
function labelvier_install_force_https() {
    if(l4_is_develop_environment()) {
        return;
    }

    $htaccess = ABSPATH . '.htaccess';
    if (file_exists($htaccess)) {
        $content = file_get_contents($htaccess);
        // check if the force https snippet is already in the .htaccess file more then once
        $occurrences = substr_count($content, '# BEGIN LABELVIER FORCE HTTPS');
        if($occurrences > 1) {
            // remove all occurrences of the force https snippet
            $content = preg_replace('/# BEGIN LABELVIER FORCE HTTPS.*# END LABELVIER FORCE HTTPS/s', '', $content);
            // remove any double empty lines
            $content = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $content);
        } else if($occurrences === 1) {
            return false;
        }
        if (!str_contains($content, 'HTTPS forced by SG-Optimizer')) {
            $https_content = <<<EOT
# BEGIN LABELVIER FORCE HTTPS
<IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteCond %{HTTPS} off
    RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>
# END LABELVIER FORCE HTTPS
EOT;
            $content = PHP_EOL . $https_content . PHP_EOL . PHP_EOL  . $content;
            return file_put_contents($htaccess, $content);
        } else {
            return false;
        }
    }
    return false;
}