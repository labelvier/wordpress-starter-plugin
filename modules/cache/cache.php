<?php
// if there is an acf option page update, run the hook
add_filter('acf/options_page/save', 'labelvier_reset_cache');

// if there is a gravity form update, run the hook
add_filter('gform_after_save_form', 'labelvier_reset_cache');
add_filter('gform_after_delete_form', 'labelvier_reset_cache');
add_filter('gform_post_form_restored', 'labelvier_reset_cache');
add_filter('gform_post_form_trashed', 'labelvier_reset_cache');
add_filter('gform_post_form_duplicated', 'labelvier_reset_cache');
add_filter('gform_post_update_form_meta', 'labelvier_reset_cache');

/**
 * Reset the cache if there is an acf option page update or a post update
 * @return void
 */
function labelvier_reset_cache() {
    if(function_exists('sg_cachepress_purge_everything')) {
        sg_cachepress_purge_everything();
    }
}
