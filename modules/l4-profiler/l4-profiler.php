<?php
/**
 * L4 Profiler
 * See which plugin causes a drop in your performance, side-plugin for Label Vier Extra's
 */

class L4_Profiler {

	function __construct() {
		add_action( 'admin_menu', [ $this, 'addAdminMenu' ] );
		add_action( 'admin_init', [ $this, 'checkMUPlugin' ] );
	}

	function addAdminMenu() {
		add_management_page( __( 'L4 Profiler', 'l4-profiler' ), __( 'L4 Profile Plugins', 'l4-profiler' ), 'manage_options', 'l4-profiler', array(
			$this,
			'ProfilerPage'
		) );
	}

	function ProfilerPage() {
		require_once __DIR__ . '/l4-profiler-page.php';
	}

	function checkMUPlugin() {
		$path = WPMU_PLUGIN_DIR . '/l4-profiler-mu.php';
		if(!is_dir(WPMU_PLUGIN_DIR)) {
			if ( ! mkdir( $concurrentDirectory = WPMU_PLUGIN_DIR ) && ! is_dir( $concurrentDirectory ) ) {
				throw new \RuntimeException( sprintf( 'Directory "%s" was not created', $concurrentDirectory ) );
			}
		}
		if(!file_exists($path)) {
			copy(__DIR__ . '/l4-profiler-mu.php', $path);
		}
	}

	function removeMUPlugin() {

	}


	/**
	 * Tmp disable plugin load (only works when this is a mu-plugin)
	 *
	 * @param $plugins
	 *
	 * @see https://deliciousbrains.com/excluding-wordpress-plugins-loading-specific-ajax-requests/
	 *
	 * @return mixed
	 */
	function exclude_plugin_from_load( $plugins ) {
		if ( !isset($_GET['disable_plugin'])) {
			return $plugins;
		}

		//disable the plugin we are about to move
		foreach($plugins as $key => $plugin) {
			$folder = explode('/', $plugin)[0];
			if($folder === $_GET['disable_plugin']) {
				unset($plugins[$key]);
				break;
			}
		}
		return $plugins;
	}
}

//Are we running in the Dashboard?
$l4_profiler = new L4_Profiler();

// remove function for MU plugin
function labelvier_remove_profiler_mu_plugin() {
	$path = WPMU_PLUGIN_DIR . '/l4-profiler-mu.php';
	unlink($path);
}
