<?php
/**
 * Plugin Name: L4 Profiler
 * Plugin URI: http://localhost
 * Description: See which plugin causes a drop in your performance, side-plugin for Label Vier Extra's
 * Author: Label Vier
 * Author URI: https://labelvier.nl
 * Version: 1.0.0
 * Tested up to: 5.0.3
 *
 */

class L4_ProfilerMustUse {

	function __construct() {
		add_filter( 'site_option_active_sitewide_plugins', [$this, 'exclude_plugin_from_load']);
		add_filter( 'option_active_plugins', [$this, 'exclude_plugin_from_load'] );
	}

	/**
	 * Tmp disable plugin load (only works when this is a mu-plugin)
	 *
	 * @param $plugins
	 *
	 * @see https://deliciousbrains.com/excluding-wordpress-plugins-loading-specific-ajax-requests/
	 *
	 * @return mixed
	 */
	function exclude_plugin_from_load( $plugins ) {
		if ( !isset($_GET['disable_plugin'])) {
			return $plugins;
		}

		//disable the plugin we are about to move
		foreach($plugins as $key => $plugin) {
			$folder = explode('/', is_numeric($plugin) ? $key : $plugin )[0];
			if($folder === $_GET['disable_plugin']) {
				unset($plugins[$key]);
				break;
			}
		}
		return $plugins;
	}
}

//Are we running in the Dashboard?
$l4_mu_profiler = new L4_ProfilerMustUse();
