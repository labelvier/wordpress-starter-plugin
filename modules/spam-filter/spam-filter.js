function isHeadless() {
    /* DISABLED to prevent false positives
    var documentDetectionKeys = [
        "__webdriver_evaluate",
        "__selenium_evaluate",
        "__webdriver_script_function",
        "__webdriver_script_func",
        "__webdriver_script_fn",
        "__fxdriver_evaluate",
        "__driver_unwrapped",
        "__webdriver_unwrapped",
        "__driver_evaluate",
        "__selenium_unwrapped",
        "__fxdriver_unwrapped",
    ];

    var windowDetectionKeys = [
        "_phantom",
        "__nightmare",
        "_selenium",
        "callPhantom",
        "callSelenium",
        "_Selenium_IDE_Recorder",
    ];

    for (const windowDetectionKey in windowDetectionKeys) {
        const windowDetectionKeyValue = windowDetectionKeys[windowDetectionKey];
        if (window[windowDetectionKeyValue]) {
            return true;
        }
    }

    for (const documentDetectionKey in documentDetectionKeys) {
        const documentDetectionKeyValue = documentDetectionKeys[documentDetectionKey];
        if (window['document'][documentDetectionKeyValue]) {
            return true;
        }
    }
     */


    if (window['external'] && window['external'].toString() && (window['external'].toString()['indexOf']('Sequentum') != -1)) return true;

    if (window['document']['documentElement']['getAttribute']('selenium')) return true;
    if (window['document']['documentElement']['getAttribute']('webdriver')) return true;
    if (window['document']['documentElement']['getAttribute']('driver')) return true;

    return false;
}

// add custom variable to commentform
const form = document.getElementById('commentform');
if (form && !isHeadless()) {
    const url = new URL(form.action);
    url.searchParams.set('is_legit', 1);
    form.action = url.toString();
} else if (isHeadless()) {
    const url = new URL(form.action);
    url.searchParams.set('is_headless', 1);
    form.action = url.toString();
}

// add custom variable to gravity forms
const gravityforms = document.querySelectorAll('.gform_wrapper form');
for (const gravityform of gravityforms) {
    addCustomVariableToGravityForms(gravityform);
}

function addCustomVariableToGravityForms(gravityform) {
    if(!gravityform) {
        return;
    }
    const target = gravityform.target ? gravityform.target : '';
    const isAjax = target.indexOf('_ajax') !== -1;
    if (!isAjax && !isHeadless()) {
        const url = new URL(gravityform.action);
        url.searchParams.set('is_legit', 1);
        gravityform.action = url.toString();
        console.log('spam-filter.js: added is_legit to gravityform');
        return;
    } else if (!isHeadless() && !gravityform.querySelector('input[name="is_legit"]')) {
        gravityform.innerHTML += '<input type="hidden" name="is_legit" value="1">';
        console.log('spam-filter.js: added is_legit to gravityform');
        return;
    } else if (isHeadless()) {
        if (!isAjax) {
            const url = new URL(gravityform.action);
            url.searchParams.set('is_headless', 1);
            gravityform.action = url.toString();
            console.log('spam-filter.js: added is_headless to gravityform');
            return;
        } else if(!gravityform.querySelector('input[name="is_headless"]')) {
            gravityform.innerHTML += '<input type="hidden" name="is_headless" value="1">';
            console.log('spam-filter.js: added is_headless to gravityform');
            return;
        }
    }
    console.log('spam-filter.js: did not add any custom variables to gravityform');
}

document.addEventListener('DOMContentLoaded', function () {
    // check if jQuery is loaded
    if (window.jQuery) {
        // add custom variable to contact form 7
        jQuery(document).on('gform_post_render', function (event, form_id, current_page) {
            // code to trigger on form or form page render
            gravityform = document.querySelector('#gform_wrapper_' + form_id + ' form');
            addCustomVariableToGravityForms(gravityform);
        });
    }
});
