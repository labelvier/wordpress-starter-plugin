<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 13-04-2021
 * Time: 09:58
 */

/**
 * Check if we have our javascript added query var. If not, mark as spam.
 *
 * @param $approved
 * @param $commentdata
 *
 * @return mixed|string
 */
function preprocess_new_comment( $approved, $commentdata ) {
	// check if we want to filter gravityforms spam
	$filter = get_option( 'options_labelvier_spam_filter_comments' );
	if ( ! $filter ) {
		return $approved;
	}

	if ( ! isset( $_GET['is_legit'] ) ) {
		// spammy?
		$approved = 'spam';

		// send debug mail
		labelvier_maybe_send_spam_debug( 'WP Comment' );
	}

	return $approved;
}

add_action( 'pre_comment_approved', 'preprocess_new_comment', 10, 2 );

/**
 * Move comment which is marked as spam directly to the spam box
 *
 * @param $commentID
 * @param $approved
 * @param $commentData
 */
function move_comment_to_spambox( $commentID, $approved, $commentData ) {
	if ( 'spam' === $approved ) {
		wp_spam_comment( $commentID );
	}
}

add_action( 'comment_post', 'move_comment_to_spambox', 100, 3 );


/**
 * Check if we have our javascript added query var. If not, mark as spam.
 *
 * @param $is_spam
 * @param $form
 * @param $entry
 *
 * @return bool
 */
function preprocess_new_gravity_forms_entry( $is_spam, $form, $entry ) {
	// check if we want to filter gravityforms spam
	$filter = get_option( 'options_labelvier_spam_filter_gravity_forms' );
	if ( ! $filter ) {
		return $is_spam;
	}

	if ( ! isset( $_GET['is_legit'] ) ) {
		// ajax fallback
		if( isset( $_POST['gform_ajax'], $_POST['is_legit'] ) ) {
			return $is_spam;
		}

		// send debug mail
		labelvier_maybe_send_spam_debug( 'Gravity Forms' );

		// spammy?
		return true;
	}

	return $is_spam;
}

add_filter( 'gform_entry_is_spam', 'preprocess_new_gravity_forms_entry', 10, 3 );

/**
 * Send a debug email when a spam entry is marked by our spam filter.
 *
 * @param string $type
 */
function labelvier_maybe_send_spam_debug( $type ) {
	if ( ! $mail = get_option( 'options_labelvier_spam_mail_notification' ) ) {
		return;
	}

	$subject = 'Nieuwe spam inzending vanaf ' . get_site_url();
	$body    = 'Hallo,
	
Zojuist is er een inzending van ' . $type . ' gemarkeerd als spam. Hieronder de inzending. Mocht dit niet kloppen dan kan je onderstaande informatie doorsturen naar hallo@labelvier.nl. Mocht de spamfilter prima werken, dan kan je je emailadres weer uitschakelen in de thema settings.

pagina: ' . print_r( $_SERVER['HTTP_REFERER'], true ) . '
form: ' . print_r( $_SERVER['REQUEST_URI'], true ) . '
browser: ' . print_r( $_SERVER['HTTP_USER_AGENT'], true ) . '

' . print_r( $_POST, true );

	wp_mail( $mail, $subject, $body );
}


