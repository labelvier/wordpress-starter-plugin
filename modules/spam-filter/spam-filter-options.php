<?php
/**
 * Created by PhpStorm.
 * User: Eric Mulder
 * Date: 13-04-2021
 * Time: 09:58
 */


add_filter( 'labelvier_theme_settings_fields', function ( $fields ) {
	$spam_fields = array(
		array(
			'key'               => 'field_5e32a9aead39d',
			'label'             => 'Spamfilter',
			'name'              => '',
			'type'              => 'tab',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'placement'         => 'top',
			'endpoint'          => 0,
		),
		array(
			'key'               => 'field_60acefe038489',
			'label'             => 'Spamfilter',
			'name'              => 'labelvier_enable_spam_filter',
			'type'              => 'true_false',
			'instructions'      => 'Zet de Label Vier spamfilter aan, deze  probeert spam inzendingen te blokkeren.',
			'required'          => 0,
			'conditional_logic' => 0,
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Spamfilter activeren',
			'default_value'     => 0,
			'ui'                => 0,
			'ui_on_text'        => '',
			'ui_off_text'       => '',
		),
		array(
			'key'               => 'field_60acf0133848a',
			'label'             => 'Spamfilter configureren',
			'name'              => 'labelvier_spam_filter_comments',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_60acefe038489',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Comments en Woocommerce reviews filteren',
			'default_value'     => 1,
			'ui'                => 0,
			'ui_on_text'        => '',
			'ui_off_text'       => '',
		),
		array(
			'key'               => 'field_60acf0453848b',
			'label'             => '&nbsp;',
			'name'              => 'labelvier_spam_filter_gravity_forms',
			'type'              => 'true_false',
			'instructions'      => '',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_60acefe038489',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'message'           => 'Gravity Forms inzendingen filteren',
			'default_value'     => 1,
			'ui'                => 0,
			'ui_on_text'        => '',
			'ui_off_text'       => '',
		),
		array(
			'key'               => 'field_60acf05a3848c',
			'label'             => 'Controle e-mail versturen',
			'name'              => 'labelvier_spam_mail_notification',
			'type'              => 'email',
			'instructions'      => 'Optioneel een controle e-mail bij een spam melding versturen. We raden aan dit tijdelijk aan te zetten om te controleren of het spamfilter naar wens werkt.',
			'required'          => 0,
			'conditional_logic' => array(
				array(
					array(
						'field'    => 'field_60acefe038489',
						'operator' => '==',
						'value'    => '1',
					),
				),
			),
			'wrapper'           => array(
				'width' => '',
				'class' => '',
				'id'    => '',
			),
			'default_value'     => '',
			'placeholder'       => 'hello@example.com',
			'prepend'           => '',
			'append'            => '',
		)
	);
	$fields            = array_merge( $fields, $spam_fields);

	return $fields;
}, 25, 1);


add_action('acf/input/admin_head', function() {
	echo '<style>.acf-field-60acf0453848b { padding-top: 0 !important; }</style>';
});
